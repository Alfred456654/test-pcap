package main;

import java.io.IOException;

public class StdinTest {
    public static void main(String[] args) throws IOException {
        // This is actually enough to make openjdk crash.........
        System.in.read();
    }
}
