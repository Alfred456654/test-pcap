package main;

import org.pcap4j.core.BpfProgram;
import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;
import org.pcap4j.packet.Packet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.concurrent.TimeoutException;

public class PcapTest {

    private static PrintWriter output;

    public static void main(String[] args) throws PcapNativeException, NotOpenException, IOException, TimeoutException {
        File outputFile = new File("broadcast_received.txt");
        Files.deleteIfExists(outputFile.toPath());
        boolean result = outputFile.createNewFile();
        if (!result) {
            System.err.println("cannot create output, exiting");
            return;
        }
        output = new PrintWriter(outputFile);
        PcapNetworkInterface device = Pcaps.getDevByAddress(InetAddress.getByName("192.168.219.140"));
        int snapshotLength = 65536;
        int readTimeoutMs = 1000;
        final PcapHandle handle = device.openLive(snapshotLength, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, readTimeoutMs);

        handle.setFilter("udp port 5910", BpfProgram.BpfCompileMode.OPTIMIZE);

        int maxPackets = 10;

        for (int i = 0; i < maxPackets; i++) {

            // This line creates the "crash"
            Packet p = handle.getNextPacketEx();
            showInfoAboutPacket(p);
        }
        handle.close();
        output.close();
    }

    private static void showInfoAboutPacket(Packet packet) {
        String msgReceived = new String(packet.getPayload().getPayload().getPayload().getRawData(), Charset.defaultCharset());
        output.println(msgReceived);
    }
}
