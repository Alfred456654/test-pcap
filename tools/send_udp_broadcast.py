import socket
from time import sleep
from datetime import datetime

# send the date every second
if __name__ == '__main__':
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.bind(('0.0.0.0', 0))
    while True:
        sock.sendto(bytes(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "ascii"), ("192.168.219.255", 5910))
        sleep(1)
